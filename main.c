#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#define MAX_ITEMS 100

void addClass(char *class_name);
void removeClass(char *class_name);
void getSubmitCli(char *class);
void getClassSubmitCli(char *class);
void parseLine(char *class, char *line);

struct assignment
{
	char *class;
	char *ID;
	char *name;
	char *due_date;
	char *due_time;
	char *cutoff_date;
	char *cutoff_time;
};

void add_item(struct assignment *a, int *num_items);
void delete_item(int *num_items, int item);
int compare(const void *p, const void *q);
void sort();
void getClasses();
void createFileSystem();
void submitFiles(char *class, char *ID);
void removeStructs(char *class_name);
void freeStructs();
void toString(struct assignment *a);
char* toCSV(struct assignment *a, char * dest);
void addAssign(char *class_name);
char * getLine(void);


struct assignment **assignments;
int items;

int main(){
	assignments = malloc(MAX_ITEMS*sizeof(struct assignment*));
	char input[16];
	do{
		/*
			This is where the user inputs a command he/she wants to do. 
		*/

		printf("Welcome. Choose a command.\n");
		printf("1. Add class.\n");
		printf("2. Remove class.\n");
		printf("3. Print current classes.\n");
		printf("4. Print all due dates.\n");
		printf("5. Print specific class.\n");
		printf("6. Create/Update File System based on Classes\n");
		printf("7. Submit assignment\n");
                printf("8. Add other Assignment\n");
                printf("9. Remove other Assignment\n");
		printf("10. Quit.\n");

		scanf("%s", input);
		printf("You entered %s\n", input);
		if(strcmp(input, "1") == 0){
			printf("Enter the class name (ex: cs283)");
			scanf("%s", input);
			addClass(input);
		}else if(strcmp(input, "2") == 0){
			printf("Enter the class name (ex: cs283)");
			scanf("%s", input);
			removeClass(input);
		}else if(strcmp(input, "3") == 0){
			FILE *file = fopen("classes.txt", "r");
			char *line = NULL;
			size_t len = 0;
			ssize_t read;
			printf("Classes: \n");
			while((read = getline(&line, &len, file)) != -1){
				printf("%s\n", line);
			}
		}else if(strcmp(input, "4") == 0){
			getClasses();
			qsort(assignments, items, sizeof *assignments, compare);
			int e;
			for(e = 0; e < items; e++){
				toString(assignments[e]);
			}
		}else if(strcmp(input, "5") == 0){
			printf("Enter the class name (ex: cs283)");
			scanf("%s", input);
			getClassSubmitCli(input);
		}else if(strcmp(input, "6") == 0){
			printf("This command will create a number of directories based\n"
					"on your current classes added. Those class directories will\n"
					"then be populated with directories refering to\t"
					"assignments.\n" 
					"Are you sure you want to do this?(y/n)\n");
			scanf("%s", input);
			if(strcmp(input, "y") == 0){
				createFileSystem();
			}
		}else if(strcmp(input, "7") == 0){
			printf("This command will submit all files to the class and\n"
					 "assignment ID specified. Continue?(y/n)\n");
			scanf("%s", input);
			if(strcmp(input, "y") == 0){
				char class[64];
				printf("Enter class: ");
				scanf("%s", class);
				printf("Enter AssignID: ");
				scanf("%s", input);
				submitFiles(class, input);
			}
		}else if(strcmp(input, "8") == 0){
			printf("Enter the class name (ex: cs283)");
			scanf("%s", input);
			addAssign(input);
		} else if(strcmp(input, "9") == 0){
			printf("Enter the class name (ex: cs283)");
			scanf("%s", input);
			getClassSubmitCli(input);
		}
	}while(strcmp(input, "10") != 0);
	freeStructs();
	return 1;
}

void getClasses(){
	//read the classes.txt file and call getSubmitCli from lines to populate
	//array

	FILE *file;
	char* line = NULL;
	size_t len = 0;
	ssize_t read;
	file = fopen("classes.txt", "r");
	
	while((read = getline(&line, &len, file)) != -1){
		//removes newline char
		line[strcspn(line, "\n")] = '\0';
		getSubmitCli(line);
	}
	fclose(file);
}

void addClass(char* class_name){
	/*
		Classes are simply stored in a text file. This function appeneds a new
		class to the file when called.
	*/
	FILE *file;
	file = fopen("classes.txt", "a");
	fprintf(file, "%s\n", class_name);
	fclose(file);
}

void addAssign(char *class_name) {
    FILE *file;
    file = fopen("assigns.txt", "w");
    struct assignment * a = NULL;
    a = malloc(sizeof(struct assignment));
    char *inp;
    a->class = strdup(class_name);
    printf("Enter assignment id: (e.g. L1): ");
    inp = getLine();
    inp[strcspn(inp, "\n")] = '\0';
    a->ID = inp;
    printf("Enter assignment name: (e.g. test): ");
    inp = getLine();
    inp[strcspn(inp, "\n")] = '\0';
    a->name = inp;
    printf("Enter assignment due date: (e.g. 01/23/2014): ");
    inp = getLine();
    inp[strcspn(inp, "\n")] = '\0';
    a->due_date = inp;
    printf("Enter assignment due time: (e.g. 23:59): ");
    inp = getLine();
    inp[strcspn(inp, "\n")] = '\0';
    a->due_time = inp;
    printf("Enter assignment cutoff date: (e.g. 01/23/2014): ");
    inp = getLine();
    inp[strcspn(inp, "\n")] = '\0';
    a->cutoff_date = inp;
    printf("Enter assignment cutoff time: (e.g. 23:59): ");
    inp = getLine();
    inp[strcspn(inp, "\n")] = '\0';
    a->cutoff_time = inp;
    char csvString[256];
    printf("%s\n",toCSV(a,csvString));

}

void removeClass(char *class_name){
	/*
		This function increments through each line in classes.txt. Every line
		is added to a temp file, unless it is the same as the class_name
		parameters, then it is ignored. 
		The original classes.txt is then removed at the end, and the temp file
		is renamed.
	*/
	FILE *file = fopen("classes.txt", "rb");
	char temp_filename[] = "temp.txt";
	FILE *temp = fopen(temp_filename, "wb");
	char line[256];
	
	while(fgets(line, 256, file) != NULL){
		if(strstr(line, class_name)){
			fprintf(temp, "%s", "");
		}else{
			fprintf(temp, "%s", line);
		}
	}
	fclose(file);
	fclose(temp);
	remove("classes.txt");
	rename(temp_filename, "classes.txt");
	removeStructs(class_name);
}

void removeStructs(char *class_name){
	/*
		removes all structs of class class_name
	*/
	int i;
	for(i = 0; i < items; i++){
		if(strcmp(assignments[i]->class, class_name) == 0){
			delete_item(&items, i);
			//decremented here if an item is deleted
			i = i - 1;
		}
	}
}

void getSubmitCli(char *class){
	/*
		Calls submit_cli using the system command. Each line is then parsed by
		parseLine, which stored each line in a struct assignment
	*/
	FILE *fp;
	int status;
	char path[1035];
	
	char command[256];
	strcpy(command, "submit_cli -c");
	strcat(command, class);
	strcat(command, " --l");

	fp = popen(command, "r");
	if(fp == NULL){
		printf("Failed to run command.\n");
		exit;
	}
	int counter = 0;
	while(fgets(path, sizeof(path), fp) != NULL){
		//these are the 3 blank lines, line_num
		//if(counter != 0 && counter != 1 && counter != 2){
		if(strcmp(path, "\n") != 0 && strcmp(path, "") != 0){
			//skip first blank line and the generic description line
			if(counter != 0 && counter != 1){
				parseLine(class, path);
			}
			counter = counter + 1;
		}
	}
	pclose(fp);
}

void getClassSubmitCli(char* class){
	/*
		Loops through the global struct array and calls toString on structs
		with the class, class
	*/
	int i;
	for(i = 0; i < items; i++){
		if(strcmp(assignments[i]->class, class) == 0){
			toString(assignments[i]);
		}
	}
}

void parseLine(char *class, char* line){
	char *ch;
	ch = strtok(line, " ");
	int counter = 0;
	//0: AssignID
	//1: AssignName
	//2: due_date
	//3: due_time
	//4: cut_off date
	//5: cut_off time

	char assignment_name[256];
	bool in_quotes = false;
	int num_words = 1;
	assignments[items] = malloc(sizeof(struct assignment));
	assignments[items]->class = strdup(class);
	//uses a str tokenizer to gets all fields seperated by " "
	while(ch != NULL){
		if(counter == 0){
			assignments[items]->ID = strdup(ch);
		}else if(counter == 1){
			//need to thread together strings for assignment name
			//assignment names are surrounded in quotes, and may have spaces in
			//them. Because of this, the program needs to check if the string
			//tokenizer is still parsing while in quotes
			in_quotes = true;
			strcpy(assignment_name, ch);
			while(in_quotes == true){
				ch = strtok(NULL, " ");
				strcat(assignment_name, " ");
				num_words = num_words + 1;
				if(strstr(ch, "\"") != NULL){
					in_quotes = false;
					strcat(assignment_name, ch);
				}else{
					strcat(assignment_name, ch);
				}
			}
			assignments[items]->name = strdup(assignment_name);
		}else if(counter == 2){
			assignments[items]->due_date = strdup(ch);
		}else if(counter == 3){
			assignments[items]->due_time = strdup(ch);
		}else if(counter == 4){
			assignments[items]->cutoff_date = strdup(ch);
		}else if(counter == 5){
			assignments[items]->cutoff_time = strdup(ch);
		}
		ch = strtok(NULL, " ");
		counter = counter + 1;
	}
	items += 1;
	//add_item(class, &items);
}

void add_item(struct assignment *a, int *num_items){
	/*
	Adds an item to the end of the global struct array
	*/
	if(*num_items < MAX_ITEMS){
		assignments[*num_items] = a;
		*num_items += 1;
	}
}

void delete_item(int *num_items, int item){
	/*
		Deletes item from global array by assigning that index to the struct
		at index+1, until the end of the array
	*/
	if(*num_items > 0 && item < *num_items && item > -1){
		int last_index = *num_items - 1;
		int i;
		for(i = item; i < last_index; i++){
			assignments[i] = assignments[i + 1];
		}
		*num_items -= 1;
	}
}

int compare(const void *p, const void *q){
	/*
		Compares by splitting the due_dates and due_times and comparing those
		between the 2 objects
	*/
	struct assignment *a = *(struct assignment**)p;
	struct assignment *b = *(struct assignment**)q;
	char *a_dueDate = a->due_date;
	char *a_dueTime = a->due_time;

	//split due_date
	int month;
	int day;
	int year;
	char *ch = strdup(a_dueDate);
	ch = strtok(ch, "/");
	month = (int)atoi(ch);
	ch = strtok(NULL, "/");
	day = (int)atoi(ch);
	ch = strtok(NULL, "/");
	year = (int)atoi(ch);
	//split due_time
	int hour;
	int minute;

	ch = strdup(a_dueTime);
	
	ch = strtok(ch, ":");
	hour = (int)atoi(ch);
	ch = strtok(NULL, ":");
	minute = (int)atoi(ch);
	
	char *b_dueDate = b->due_date;
	char *b_dueTime = b->due_time;

	//split b_dueDate
	int b_month;
	int b_day;
	int b_year;

	ch = strdup(b_dueDate);

	ch = strtok(ch, "/");
	b_month = (int)atoi(ch);
	ch = strtok(NULL, "/");
	b_day = (int)atoi(ch);
	ch = strtok(NULL, "/");
	b_year = (int)atoi(ch);
	
	int b_hour;
	int b_minute;

	ch = strdup(b_dueTime);

	ch = strtok(ch, ":");
	hour = (int)atoi(ch);
	ch = strtok(NULL, ":");
	minute = (int)atoi(ch);

	//now we compare
	if(year < b_year){
		return -1;
	}else if(year > b_year){
		return 1;
	}else{
		if(month < b_month){
			return -1;
		}else if(month > b_month){
			return 1;
		}else{
			if(day < b_day){
				return -1;
			}else if(day > b_day){
				return 1;
			}else{
				if(hour < b_hour){
					return -1;
				}else if(hour > b_hour){
					return 1;
				}else{
					if(minute < b_minute){
						return -1;
					}else if(minute > b_minute){
						return 1;
					}else{
						return 0;
					}
				}
			}
		}
	}
	return 0;
}

void createFileSystem(){
	/*
	
		Creates a bunch of folders named the current classes, and populates
		those folders with assignments ID folders. The purpose of this is to
		be able to submit directly from the program if files are in those ID
		folders
	*/
	FILE *file;
	char *line = NULL;
	size_t len = 0;
	ssize_t read;
	file = fopen("classes.txt", "r");
	struct stat st = {0};

	while((read = getline(&line, &len, file)) != -1){
		//each line in classes.txt corresponds to a class(ex: cs283, cs275)
		if(stat(line, &st) == -1){
			line[strlen(line)-1]=0;
			mkdir(line, 0700);
		}
	}
	int i;
	for(i = 0; i < items; i++){
		char loc[16];
		//create directory based on class, ID
		strcat(loc, assignments[i]->class);
		strcat(loc, "/");
		strcat(loc, assignments[i]->ID);
		//printf("loc %s\n", loc);
		if(stat(loc, &st) == -1){
			//printf("MKDIR at %s\n", loc);
			mkdir(loc, 0700);
		}
		strcpy(loc, "");
	}
}

void submitFiles(char *class, char *ID){
	/*
		Submits files contained within the directory located in /class/ID
	*/
	char loc[64];
	strcpy(loc, "./");
	strcat(loc, class);
	strcat(loc, "/");
	strcat(loc, ID);

	DIR *dir;
	struct dirent *ent;

	if((dir = opendir(loc)) != NULL){
		char submit_str[256];
		strcpy(submit_str, "submit_cli -c");
		strcat(submit_str, class);
		strcat(submit_str, " -a ");
		strcat(submit_str, ID);
		while((ent = readdir(dir)) != NULL){
			//ignore dirent entries of .. and .
			if(strcmp(ent->d_name, "..") != 0 && strcmp(ent->d_name, ".") != 0){
				strcat(submit_str, " ");
				strcat(submit_str, ent->d_name);
			}
		}
		FILE *fp;
		int status;
		char path[1035];
		fp = popen(submit_str, "r");
		pclose(fp);

		closedir(dir);
	}else{
		printf("Couldn't find/open directory: %s\n", loc);
	}	
}

void toString(struct assignment *a){
	printf("CLASS: %s ID: %s	Name: %s	Due Date: 	%s %s	Cut Off:	%s %s",
			a->class, a->ID, a->name, a->due_date, a->due_time, a->cutoff_date,
			a->cutoff_time);
}

void freeStructs(){
	int i;
	for(i = 0; i < items; i++){
		free(assignments[i]);
	}
	free(assignments);
}

char * getLine(void) {
    char * line = malloc(100), * linep = line;
    size_t lenmax = 100, len = lenmax;
    int c;

    if(line == NULL)
        return NULL;

    for(;;) {
        c = fgetc(stdin);
        if(c == EOF)
            break;

        if(--len == 0) {
            len = lenmax;
            char * linen = realloc(linep, lenmax *= 2);

            if(linen == NULL) {
                free(linep);
                return NULL;
            }
            line = linen + (line - linep);
            linep = linen;
        }

        if((*line++ = c) == '\n')
            break;
    }
    *line = '\0';
    return linep;
}

char* toCSV(struct assignment *a, char * dest){
    strcpy(dest, a->class);
    strcpy(dest, ",");
    strcpy(dest, a->ID);
    strcpy(dest, ",");
    strcpy(dest, a->name);
    strcpy(dest, ",");
    strcpy(dest, a->due_date);
    strcpy(dest, ",");
    strcpy(dest, a->due_time);
    strcpy(dest, ",");
    strcpy(dest, a->cutoff_date);
    strcpy(dest, ",");
    strcpy(dest, a->cutoff_time);
    return dest;
}
